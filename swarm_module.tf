module "swarm" {
  source                 = "git::ssh://git@bitbucket.org/falabellafif/swarm-azure-terraform.git//test-qa?ref=v1.8.0"
  environment            = "${var.environment}"
  slug                   = "${var.slug}"
  network_name           = "${module.vnet-fif-terraform.network_name}"
  resource_group_network = "${module.vnet-fif-terraform.resource_group_network}"
  sub_network            = "${var.subnet_network}"
  location               = "${module.vnet-fif-terraform.location}"
  chef_role              = "${var.slug}"
  path_fif_validator     = "${var.path_fif_validator}"
  ip_public_name         = "${var.ip_public_name}"
}
