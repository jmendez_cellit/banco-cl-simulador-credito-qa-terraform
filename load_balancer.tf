# LB Public
module "lb_rule_Kong_public_443" {
  source                  = "git::ssh://git@bitbucket.org/falabellafif/module-add-lb-rules-terraform.git"
  resource_group_name     = "${module.swarm.resource_group_name}"
  loadbalancer_id         = "${module.swarm.public_loadbalancer_id}"
  frontend_port           = "443"
  backend_port            = "8443"
  service_name            = "${var.slug}-kong-public-443-${var.environment}"
  backend_address_pool_id = "${module.swarm.public_lb_backend_address_pool_id}"
  ip_name                 = "${var.slug}-ip-${var.environment}-${element(var.ip_public_name, 0)}"
}

# LB Private
module "lb_rule_Kong_private_443" {
  source                  = "git::ssh://git@bitbucket.org/falabellafif/module-add-lb-rules-terraform.git"
  resource_group_name     = "${module.swarm.resource_group_name}"
  loadbalancer_id         = "${module.swarm.private_loadbalancer_id}"
  frontend_port           = "443"
  backend_port            = "8443"
  service_name            = "${var.slug}-kong-private-443-${var.environment}"
  backend_address_pool_id = "${module.swarm.private_lb_backend_address_pool_id}"
  ip_name                 = "${var.slug}-private-ip-${var.environment}"
}

module "lb_rule_Kong_private_8444" {
  source                  = "git::ssh://git@bitbucket.org/falabellafif/module-add-lb-rules-terraform.git"
  resource_group_name     = "${module.swarm.resource_group_name}"
  loadbalancer_id         = "${module.swarm.private_loadbalancer_id}"
  frontend_port           = "8444"
  backend_port            = "8444"
  service_name            = "${var.slug}-kong-private-8444-${var.environment}"
  backend_address_pool_id = "${module.swarm.private_lb_backend_address_pool_id}"
  ip_name                 = "${var.slug}-private-ip-${var.environment}"
}
